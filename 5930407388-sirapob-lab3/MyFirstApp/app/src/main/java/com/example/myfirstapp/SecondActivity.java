package com.example.myfirstapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class SecondActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);

        Intent intent = getIntent();
        String name = ((Intent) intent).getStringExtra("name");
        String phone = intent.getStringExtra("phone");

        TextView showText = (TextView) findViewById(R.id.show_tv);

        showText.setText(name + " has phone number as " + phone);
    }
}
