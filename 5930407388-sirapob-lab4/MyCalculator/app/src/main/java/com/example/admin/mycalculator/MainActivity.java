package com.example.admin.mycalculator;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

public class MainActivity extends AppCompatActivity implements View.OnClickListener, RadioGroup.OnCheckedChangeListener {
    EditText first_num;
    EditText second_num;
    TextView result;
    RadioGroup group_rd;
    RadioButton add_rd;
    RadioButton sub_rd;
    RadioButton multi_rd;
    RadioButton div_rd;
    Button cal_btn;
    Switch sw_btn;
    Context context;
    TextView tv_sw;
    float first_val;
    float second_val;

    final int TOASTS_DURATION = Toast.LENGTH_SHORT;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        inItInstances();

        sw_btn.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked) {
                    tv_sw.setText("ON");
                } else
                    tv_sw.setText("OFF");
            }
        });
    }

        private void inItInstances() {
            first_num = (EditText) findViewById(R.id.first_number);
            second_num = (EditText) findViewById(R.id.second_number);
            result = (TextView) findViewById(R.id.result);
            add_rd = (RadioButton) findViewById(R.id.add_rd);
            sub_rd = (RadioButton) findViewById(R.id.sub_rd);
            multi_rd = (RadioButton) findViewById(R.id.multi_rd);
            div_rd = (RadioButton) findViewById(R.id.div_rd);
            cal_btn = (Button) findViewById(R.id.cal_btn);
            cal_btn.setOnClickListener(this);
            group_rd = (RadioGroup) findViewById(R.id.group_rd);
            sw_btn = (Switch) findViewById(R.id.sw_btn);
            group_rd.setOnCheckedChangeListener(this);
            context = getApplicationContext();
            tv_sw = (TextView) findViewById(R.id.tv_sw);
        }

    private void acceptNumbers() {
        try {
            first_val = Float.parseFloat(first_num.getText().toString());
            second_val = Float.parseFloat(second_num.getText().toString());
        } catch (NumberFormatException e) {
            showToast("Please enter only a number");
        }
    }

    private void calculate(int i) {
        long start = System.currentTimeMillis();
        float res;
        acceptNumbers();

        switch(i) {
            case R.id.add_rd:
                res = first_val + second_val;
                break;
            case R.id.sub_rd:
                res = first_val - second_val;
                break;
            case R.id.multi_rd:
                res = first_val * second_val;
                break;
            case R.id.div_rd:
                if(second_val == 0) {
                    showToast("Please divide by a non-zero number");
                    return;
                }
                res = first_val / second_val;
                break;
            default:
                return;
        }
        result.setText("= " + Float.toString(res));
        long runTime = System.currentTimeMillis() - start;
        Log.d("Calculation", "computation time = " + Float.toString((((float)runTime))/1000.0f)); // tag of log
    }

    @Override
    public void onClick(View v) {
        if (v == cal_btn) {
            calculate(group_rd.getCheckedRadioButtonId());
        }
    }

    private void showToast(String str) {
        Toast toast = Toast.makeText(context, str, TOASTS_DURATION);
        toast.setGravity(Gravity.BOTTOM, 0, 100);
        toast.show();

    }


    @Override
    public void onCheckedChanged(RadioGroup group, int checkedId) {
        calculate(checkedId);
    }

}
