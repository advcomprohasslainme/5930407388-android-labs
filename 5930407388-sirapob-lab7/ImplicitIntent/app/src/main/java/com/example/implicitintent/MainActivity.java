package com.example.implicitintent;

import android.content.Intent;
import android.net.Uri;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    Button intentImplicit;
    Spinner spinner;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        intentImplicit = (Button)findViewById(R.id.implicitBtn);
        intentImplicit.setOnClickListener(this);

        spinner = (Spinner)findViewById(R.id.spinner);
        ArrayAdapter adapter = ArrayAdapter.createFromResource(this, R.array.intents, android.R.layout.simple_spinner_item);

        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
    }

    @Override
    public void onClick(View v) {
        if (v == intentImplicit) {
            int pos = spinner.getSelectedItemPosition();
            String intentAction = "";
            Uri intentURI = null;
            switch (pos) {
                case 0:
                    intentAction = Intent.ACTION_VIEW;
                    intentURI = Uri.parse("http://m.kku.ac.th/");
                    break;
                case 1:
                    intentAction = Intent.ACTION_DIAL;
                    intentURI = Uri.parse("tel:043009700");
                    break;
                case 2:
                    intentAction = Intent.ACTION_VIEW;
                    intentURI = Uri.parse("geo:0.0?q=Khon Kaen University");
                    break;
                case 3:
                    intentAction = Intent.ACTION_VIEW;
                    intentURI = Uri.parse("content://contacts/people");
                    break;
                default:
                    break;
            }
            if(intentAction != "" && intentURI != null)
                startActivity(new Intent(intentAction, intentURI));
        }
    }
}
